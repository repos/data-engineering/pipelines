import argparse
import sys

from pyspark.sql import SparkSession
from pyspark.sql.functions import lit


def parse_args(args) -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description="""
            Efficiently parses a large set of edges into a list of vertices.
            Each vertex has a list of paths that lead up to it and knows if it's in a cycle.
        """
    )
    parser.add_argument('--snapshot', help='snapshot of source tables to be read', required=True)

    return parser.parse_args(args)


def build_graph(
        spark,
        snapshot) -> None:

    cl = spark.sql(f"""
     select cl_from,
            cl_to,
            cl_from_type
       from milimetric.commons_categorylinks
    """).repartition(512)

    edges = cl.select(
        cl.cl_from.alias('dst'),
        cl.cl_to.alias('src'),
    )

    vertices = cl.select(
        cl.cl_from.alias('id'),
        cl.cl_from_type.alias('type')
    ).unionAll(cl.select(
        cl.cl_to.alias('id'),
        lit('subcat').alias('type'),
    ))


def main() -> None:
    args = parse_args(sys.argv[1:])

    spark = SparkSession.builder.getOrCreate()

    build_graph(
        spark,
        args.snapshot
    )

    spark.stop()
