# pipelines

One idea for where to host all our logic for data pipelines.  Shared repositories are a great way to maximize sharing and collaboration.  This repository, if adopted, could contain sql, java, python, and scala logic that transforms all our data at wmf.
